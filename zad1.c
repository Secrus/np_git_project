#include <stdio.h>	// deklaracja bibliotek
#include <stdlib.h>

int main()		// funkcja główna
{
	int liczba = 0;	// deklaracja zmiennej liczba typu int oraz przypisanie jej wartości 0

	//wypisanie tekstu oraz wczytanie wartości całkowitej (%d) z klawiatury

   	printf ("Podaj liczbe calkowita:\n");
   	scanf ("%d", &liczba);

	// instrukcja warunkowa if przyrównująca 5 bit (4 z przesunięciem) do 1

   	if ((liczba>>4)%2 == 1)
   	{
   		printf("Bit nr 5 ma wartosc 1\n");	// wypisanie wyniku ‘true’
   	}
   	else
   	{
   		printf("Bit nr 5 ma wartosc 0\n");	// wypisanie wyniku ‘false’
	}
	
	// instrukcja warunkowa przyrównująca 10 bit do 1

	if ((liczba>>10)%2 == 1)
   	{
   		printf ("Bit nr 11 ma wartosc 1\n");	// wypisanie wyniku ‘true’
   	}
   	else
   	{
   		printf("Bit nr 11 ma wartosc 0\n");	// wypisanie wyniku ‘false’
   	}
   	return 0;
}
