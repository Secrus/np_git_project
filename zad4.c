#include <stdio.h>

int main(int argc, char* argv[]){
    int min;
    int max;
    int i = 0;
    int entered;
    printf("Podaj %d liczbe: ", ++i);
    scanf("%d", &entered);
    min = entered;
    max = entered;
    for(;i < 3; i++){
        printf("Podaj %d liczbe: ", (i + 1));
        scanf("%d", &entered);
            if(entered > max){
            max = entered;
        }
        if(entered < min){
            min = entered;
        }
    }
    printf("Minimalna = %d\nMaksymalna = %d\n", min, max);
    return 0;
}